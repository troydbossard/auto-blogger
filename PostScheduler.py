import time
import datetime
import multiprocessing
import mysql.connector
from typing import Tuple
from RedditAPI import create_reddit_instance, create_new_post
from gpt_to_mysql import (generate_posts_for_prompt, 
                          insert_into_prompt_table, 
                          insert_into_post_table)


def connect_to_db() -> Tuple[mysql.connector.connection.MySQLConnection, mysql.connector.cursor.MySQLCursor]:
    # Connect to MySQL database and return connection and cursor objects
    connection = mysql.connector.connect(
        host="localhost",
        user="root",
        password="admin",
        database="sys"
    )
    cursor = connection.cursor()
    return connection, cursor

connection, cursor = connect_to_db()
reddit = create_reddit_instance()

def checkDB(connection, cursor, reddit):
    curDateTime = datetime.datetime.now().replace(second=0, microsecond=0)

    query = "SELECT * FROM post"
    cursor.execute(query,)

    rows = cursor.fetchall()
    if rows:
        for row in rows:
            rowID = row[0]
            post_text = row[1]

            query = "SELECT * FROM prompt WHERE prompt_id = %s"
            cursor.execute(query, (rowID,))
            prompt_row = cursor.fetchone()

            if prompt_row:
                post_title = prompt_row[1]

                # Insert the post into the post_archive table
                archive_query = "INSERT INTO post_archive (prompt_text, post_text) VALUES (%s, %s)"
                cursor.execute(archive_query, (post_title, post_text))

                # Delete the post from the post and prompt tables
                post_query = "DELETE FROM post WHERE post_id = %s"
                cursor.execute(post_query, (rowID,))

                prompt_query = "DELETE FROM prompt WHERE prompt_id = %s"
                cursor.execute(prompt_query, (rowID,))

                # Commit the changes
                connection.commit()

                # Make the post on Reddit
                reddit = create_reddit_instance()
                create_new_post(reddit, post_title, post_text)



def runScheduler(reddit): #checks for post every minute
    connection, cursor = connect_to_db()
    while True:
        checkDB(connection, cursor, reddit)
        time.sleep(60) 

def main():
    # Create a new process for the scheduler, so it runs independently of the other processes
    scheduler_process = multiprocessing.Process(target=runScheduler, args=(reddit,))
    scheduler_process.start()
    #Prompt the user for a subject area and generate posts
    root_prompt = input("Enter a single line subject area to generate posts for, \npress enter for random prompt or \ntype -1 to continue with checking for automated posts.\n")
    if root_prompt != -1:
        posts = generate_posts_for_prompt(root_prompt)
        # Insert the posts into the database
        i= 1000
        for i, pair in enumerate(list(posts)):
            prompt_id = i
            prompt_text = pair[1][0]
            post_id = i
            post_text = pair[1][1]
            insert_into_prompt_table(cursor, prompt_id, prompt_text)
            insert_into_post_table(cursor, post_id, post_text)
    # Commit the changes and close the connection
    connection.commit()
    main()

if __name__ == '__main__':
    main()
