"""
 █████  ██    ██ ████████  ██████      ██████  ██       ██████   ██████   ██████  ███████ ██████  
██   ██ ██    ██    ██    ██    ██     ██   ██ ██      ██    ██ ██       ██       ██      ██   ██ 
███████ ██    ██    ██    ██    ██     ██████  ██      ██    ██ ██   ███ ██   ███ █████   ██████  
██   ██ ██    ██    ██    ██    ██     ██   ██ ██      ██    ██ ██    ██ ██    ██ ██      ██   ██ 
██   ██  ██████     ██     ██████      ██████  ███████  ██████   ██████   ██████  ███████ ██   ██ 

"""
##################################
Setting up your own OpenAI API key
##################################
Instructions to get started using the Python OpenAI API can be found here.

https://platform.openai.com/docs/api-reference/introduction?lang=python

You will need to install the OpenAI library on your machine in order for the OpenAI import to function.
This is handled by the virtual environment below. Optionally, use the command:
'pip install openai'

On the page above you can create your API key, which you will need to input in the designated area of 
the chatGPT_api.py file in order to send and receive information from the API.

##########################
Setting up your Reddit API
##########################
You will your client_id, client_secret, username and password, subreddit to post to and user_agent name
in order to begin making posts on Reddit. Please populate the above information into RedditAPI.py
to be able to start making posts to Reddit. 
https://www.reddit.com/dev/api/

#############################################################
Initializing the virutal environment to satisfy dependencies
#############################################################
#First install virtualenv:
'pip install virtualenv'

Then activate the virtual environment 
windows:
'venv\Scripts\activate'

if you receive an error about unauthorized access, open windows powershell as admin and type command 
'Get-ExecutionPolicy'
If it says restricted this means scripts are disabled on your machine, which won't allow venvs to work.
Use the below command to change the policy and fix this error.
'Set-ExecutionPolicy RemoteSigned'

after running the activate command you should now see venv reflected in the path in terminal:
->>>>>(venv) PS C:\Users\user\drive\files\Auto-Blogger>

now install the required dependencies using this command, replacing path/to with the correct filepath:
'pip install -r path/to/requirements.txt'

########################
Setting up your database
########################
This project relies on MySQL database integration. In order to use this program,
please install both MySQL Server as well as MySQL Workbench.
https://dev.mysql.com/downloads/mysql/
Set up your database and update your database information in the PostScheduler.py file
Run the create table statements included in the Auto-Blogger file directory within MySQL workbench.

####################
Running Auto-Blogger
####################
To run Auto-Blogger simply run PostScheduler.py and have fun.
