import praw

def create_reddit_instance() -> praw:
    # Create a Reddit instance using your API credentials
    return praw.Reddit(client_id='CLIENT_ID_HERE',
                     client_secret='CLIENT_SECRET_HERE',
                     username='USERNAME_HERE',
                     password='PASSWORD_HERE',
                     user_agent='USER_AGENT_HERE')

def create_new_post(reddit, post_title, post_text):
    subreddit = reddit.subreddit('SUBREDDIT_NAME_HERE')
    subreddit.submit(post_title, selftext=post_text)

