import mysql.connector
from chatGPT_api import generate
from datetime import datetime

def insert_into_prompt_table(cursor: mysql.connector.cursor.MySQLCursor, prompt_id: int, prompt_text: str) -> None:
    # Insert a new prompt into the 'prompt' table
    insert_query = "INSERT INTO prompt (prompt) VALUES (%s)"
    values = (prompt_text,)
    cursor.execute(insert_query, values)

def insert_into_post_table(cursor: mysql.connector.cursor.MySQLCursor, post_id: int, post_text: str) -> None:
    # Insert a new post into the 'post' table
    insert_query = "INSERT INTO post (post_text, post_date, post_time) VALUES (%(post_text)s, %(post_date)s, %(post_time)s)"
    now = datetime.now()
    # Format the date as YYYY-MM-DD
    date_str = now.strftime('%Y-%m-%d')
    # Format the time as HH:MM:SS
    time_str = now.strftime('%H:%M:%S')
    values = {#'post_id': int(post_id), 
              'post_text': post_text, 'post_date': date_str, 'post_time': time_str}
    cursor.execute(insert_query, values)


def generate_posts_for_prompt(prompt: str = "") -> list[int, list[str,str]]:
    # Generate a dictionary of posts for a given prompt
    if len(prompt) != 0:
        posts = generate(prompt)
    else:
        posts = generate()
    return posts
